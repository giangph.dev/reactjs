export const themeColors = {
    primary: '#f5a128',
    secondary: '#6E759F',
    success: '#3192bc',
    warning: '#FFA319',
    error: '#FF1943',
    info: '#33C2FF',
    black: '#223354',
    white: '#ffffff',
    primaryAlt: '#000C57'
};