import { Suspense, lazy } from 'react';

import BaseLayout from 'src/layouts/BaseLayout';

import SuspenseLoader from 'src/components/SuspenseLoader';
import SidebarLayout from 'src/layouts/SidebarLayout';
import Home from 'src/content/home';

// Page
import Course from 'src/content/course';
import Class from 'src/content/class';
import Badge from 'src/content/badge';
import Teacher from 'src/content/teacher';
import Student from 'src/content/student';
import Department from 'src/content/department';
import Position from 'src/content/position';
import Role from 'src/content/role';
import Permission from 'src/content/permission';
import TeacherExternal from 'src/content/teacherExternal';
import QuestionBank from 'src/content/questionBank';
import BadgeExternal from 'src/content/badgeExternal';
import Exam from 'src/content/exam';
import Account from 'src/content/account';
// End page

const Loader = (Component: any) => (props: any) => (
  <Suspense fallback={<SuspenseLoader />}>
    <Component {...props} />
  </Suspense>
);

//Auth
const Login = Loader(lazy(() => import('src/content/pages/Login')));
const LoginAuth = Loader(lazy(() => import('src/content/pages/Login/loginAuth'))); // use for SSO Login
// Status
const Status404 = Loader(lazy(() => import('src/content/pages/Status/Status404')));
const Status500 = Loader(lazy(() => import('src/content/pages/Status/Status500')));
const StatusComingSoon = Loader(lazy(() => import('src/content/pages/Status/ComingSoon')));
const StatusMaintenance = Loader(lazy(() => import('src/content/pages/Status/Maintenance')));

const routes = [
  {
    path: 'auth',
    element: <LoginAuth />
  },
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/status',
    element: (
      <BaseLayout />
    ),
    children: [
      {
        path: '404',
        element: <Status404 />
      },
      {
        path: '500',
        element: <Status500 />
      },
      {
        path: 'maintenance',
        element: <StatusMaintenance />
      },
      {
        path: 'coming-soon',
        element: <StatusComingSoon />
      }
    ]
  },
  {
    path: '/',
    element: (
      <SidebarLayout />
    ),
    children: [
      {
        path: 'account',
        element: <Account />
      },
      {
        path: 'home',
        element: <Home />
      },
      {
        path: 'course',
        element: <Course />
      },
      {
        path: 'class',
        element: <Class />
      },
      {
        path: 'teacher',
        element: <Teacher />
      },
      {
        path: 'student',
        element: <Teacher />
      },
      {
        path: 'department',
        element: <Department />
      },
      {
        path: 'position',
        element: <Position />
      },
      {
        path: 'role',
        element: <Role />
      },
      {
        path: 'permission',
        element: <Permission />
      },
      {
        path: 'badge',
        element: <Badge />
      },
      {
        path: 'student',
        element: <Student />
      },
      {
        path: 'question',
        element: <QuestionBank />
      },
      {
        path: 'external-badge',
        element: <BadgeExternal />
      },
      {
        path: 'external-teacher',
        element: <TeacherExternal />
      },
      {
        path: 'exam',
        element: <Exam />
      }
    ]
  },
];

export default routes;
