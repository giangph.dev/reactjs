import { Box, Container, Typography } from '@mui/material';
import { styled } from '@mui/material/styles';

const FooterWrapper = styled(Box)(
  ({ theme }) => `
  padding: ${theme.spacing(0, 2, 2, 2)};
  .MuiBox-root {
    background-color: ${theme.colors.alpha.white[100]};
    border-radius: 10px;
    padding: ${theme.spacing(2)}
  }
`
);

function Footer() {
  return (
    <FooterWrapper>
      <Box>
        <Typography variant="subtitle1">
          &copy;  {new Date().getFullYear()} - Build by Small Team
        </Typography>
      </Box>
    </FooterWrapper>
  );
}

export default Footer;
