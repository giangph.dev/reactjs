import loadable from '../Loadable';

export const Table = loadable(
  () => import('./Index')
);
