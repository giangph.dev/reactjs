import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useTranslation, withTranslation } from 'react-i18next';
interface confirmProps {
  title: string;
  content: string;
  open: any;
  handleClose: any;
  t: any;
}
function ModalConfirm(props: confirmProps) {
  const {title, content, open, handleClose, t, ...dataProps} = props;
  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          {title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            {t('action.cancel')}
          </Button>
          <Button onClick={() => handleClose('ok')}>{t('action.confirm')}</Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default  withTranslation()(ModalConfirm);
