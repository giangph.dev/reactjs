import { memo, FC } from 'react';
import { Box, TextField, Typography } from '@mui/material';
import { withTranslation } from 'react-i18next';
import { DateTimePicker, DesktopDatePicker, MobileDatePicker, TimePicker } from '@mui/lab';
interface DateTimeProps {
  value?: any;
  onChange: any;
  type?: string;
  label?: string;
  size?: any;
  sx?: any;
  name?: any;
  noLable?: any;
  placeholder?: string;
}

const render = (type: string = "", sx: any = {}, size: any = "small", onChange: any, propsData: any, value: any): JSX.Element => {
  switch(type) {
    case "dateTime":
      return <DateTimePicker
        {...propsData}
        value={value}
        onChange={onChange}
        renderInput={(params) => <TextField sx={sx} size={size} {...params} />}
      />
    case "time":
      return <TimePicker
        {...propsData}
        value={value}
        onChange={onChange}
        renderInput={(params) => <TextField sx={sx} size={size} {...params} />}
      />
    case "dateM":
      return <MobileDatePicker
        {...propsData}
        value={value}
        onChange={onChange}
        renderInput={(params) => <TextField sx={sx} size={size} {...params} />}
      />
    default:
      return <DesktopDatePicker
        {...propsData}
        value={value}
        onChange={onChange}
        renderInput={(params) => <TextField sx={sx} size={size} {...params} />}
      />
  }
}

const DateTimeContainer: FC<DateTimeProps> = memo(props => {
  const {type, label, sx, size, value, onChange, noLable, ...propsData } = props;
  return (
    <Box sx={{ display: 'flex' }}>
      {!noLable && <Typography sx={{ width: '30%', fontWeight: 'bold' }} mt={1}>{label}</Typography>}
      <Box sx={{width: "70%"}}>
        {render(type, sx, size, onChange, propsData, value)}
      </Box>
    </Box>
  )
})

export default withTranslation()(DateTimeContainer);
