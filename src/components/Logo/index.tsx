import { Box, Hidden, Tooltip } from '@mui/material';
import { Link } from 'react-router-dom';
import { styled } from '@mui/material/styles';

const LogoWrapper = styled(Link)(
  ({ theme }) => `
    color: ${theme.palette.text.primary};
    padding: ${theme.spacing(0, 0, 0, 0)};
    display: flex;
    text-decoration: none;
`
);

const LogoTextWrapper = styled(Box)(
  ({ theme }) => `
  padding-left: ${theme.spacing(1)};
`
);

const VersionBadge = styled(Box)(
  ({ theme }) => `
  padding: ${theme.spacing(0.4, 0)};
  border-radius: ${theme.general.borderRadiusSm};
  text-align: center;
  display: inline-block;
  line-height: 1;
  font-size: ${theme.typography.pxToRem(11)};
  font-weight: 400;
`
);

const LogoText = styled(Box)(
  ({ theme }) => `
  font-size: ${theme.typography.pxToRem(15)};
  font-weight: ${theme.typography.fontWeightBold};
  padding: ${theme.spacing(0.4, 0)};
`
);

function Logo() {

  return (
    <LogoWrapper to="/overview">
      <Hidden smDown>
        <LogoTextWrapper>
          <LogoText>Admin</LogoText>
          <Tooltip title="Version 1.1.0" arrow placement="right">
            <VersionBadge>Version: 1.1</VersionBadge>
          </Tooltip>
        </LogoTextWrapper>
      </Hidden>
    </LogoWrapper>
  );
}

export default Logo;
