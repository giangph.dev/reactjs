import { 
  Box,
  Paper,
} from '@mui/material';
import { useTranslation } from 'react-i18next';

interface propsAlert{
  title: string;
  content: string;
  action: any[];
}

function Alert(props: propsAlert) {
  const { title, ...propsData } = props;
  const { t } = useTranslation();
  
  return (
    <Box sx={{ borderRadius: "6px" }}>
      <Paper>
        
      </Paper>
    </Box>
  );
}

export default Alert;
