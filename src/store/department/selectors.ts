import { createSelector } from '@reduxjs/toolkit';

const department = ( state: any ) => state.department
const departmentSelector = createSelector(department, department => department);

export const dataSelector:any = createSelector(
  [departmentSelector],
  department => department.data
);


