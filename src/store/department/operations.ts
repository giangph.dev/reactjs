import createOperation from '../createOperation';
import slice from './slice';
import { services } from 'src/services/department.service';
const { actions } = slice;

export const getDepartment = createOperation({
  actions: {
    successAction: actions.getDepartmentSuccess,
  },
  async process({payload: data}) {
    return await services.getDepartment(data);
  },
});

