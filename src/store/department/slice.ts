import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    data: [],
}
export default createSlice({
    name: 'department',
    initialState,
    reducers: {
        // getDepartmentSuccess: (state, action) => {
        //     console.log(state, action)
        // },
        getDepartmentSuccess: (state, action) => ({
            ...state,
            data: action.payload?.result?.data
        }),
    }
});