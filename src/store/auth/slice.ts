import { createSlice } from "@reduxjs/toolkit";

const auth = localStorage.getItem("accessToken");
const initialState = {
    isLoggedIn: auth ? true : false,
    isError: false,
    message: 'The application is fetching data from Azure Cloud, please wait a few minutes....'
}

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        loginFulfilled: (state, action) => ({
            ...state,
            isLoggedIn : true,
            message: ""
        }),
        loginRejected: (state, action) => ({
            ...state,
            isLoggedIn : false,
            isError: true,
            message: 'An error occurred while retrieving data from Azure Cloud, please contact the administrator'
        }),
        logoutFulfilled: (state, action) => ({
            ...state,
            isLoggedIn : false,
        }),
        clearState: (state) => ({
            ...state,
            isLoggedIn: false,
            isError: false,
            message: ""
        })
    }
});

export default authSlice;
