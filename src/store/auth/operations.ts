import createOperation from '../createOperation';
import slice from './slice';
import { login } from 'src/services/auth.service';

const { actions } = slice;

export const ssoLogin = createOperation({
  actions: {
    successAction: actions.loginFulfilled,
  },
  async process() {
    return null;
  },
});

export const loginAuth = createOperation({
  actions: {
    successAction: actions.loginFulfilled,
    failedAction: actions.loginRejected
  },
  async process({payload: data}) {
    console.log(1);
    return await login(data);
  },
  onSuccess: "Login trus",
  onError: "Login Error",
  successMessage: "LoggedInSuccessfully",
  errorMessage: "LoggedInFailed"
});
