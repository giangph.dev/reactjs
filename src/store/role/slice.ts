import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isOpenSidebar: true,
}
export default createSlice({
    name: 'app',
    initialState,
    reducers: {
        toggleCollapseSideBar: (state) => ({
            ...state,
            isOpenSidebar: !state.isOpenSidebar
        }),
    }
});