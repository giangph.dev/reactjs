import { ReactNode } from 'react';

import DesignServicesTwoToneIcon from '@mui/icons-material/DesignServicesTwoTone';
import BrightnessLowTwoToneIcon from '@mui/icons-material/BrightnessLowTwoTone';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import ModelTrainingIcon from '@mui/icons-material/ModelTraining';
import AdminPanelSettingsOutlinedIcon from '@mui/icons-material/AdminPanelSettingsOutlined';
import { ROLE_MENU } from 'src/constants/roleMenu';

export interface MenuItem {
  id: string
  parent: string;
  name: string;
  role: any[];
  items?: MenuItem[];
  link?: string;
  icon?: ReactNode;
  badge?: string;
  canActive?: string[];
}

export interface MenuItems {
  id: string,
  heading: string;
  role: any[]
  items: MenuItem[];
}

const menuItems: MenuItems[] = [
  {
    id: '0',
    heading: 'Khóa Học',
    role: ["admin"],
    items: [
      {
        id: '0_1',
        parent: '0',
        name: 'Quản lý khóa học',
        role: ["admin"],
        canActive: ["/course", "/class", "/exam", "/question"],
        items: [
          {
            id: '0_2',
            parent: '0',
            name: 'Khóa học',
            link: '/course',
            role: ["admin"]
          },
          {
            id: '0_2',
            parent: '0',
            name: 'Lớp học',
            link: '/class',
            role: ["admin"]
          },
          {
            id: '0_3',
            parent: '0',
            name: 'Ký thi',
            link: '/exam',
            role: ["admin"]
          },
          {
            id: '0_4',
            parent: '0',
            name: 'Thư viện câu hỏi',
            link: '/question',
            role: ["admin"]
          }
        ]
      },
      {
        id: '0_2',
        parent: '0',
        name: 'Quản lý giảng viên',
        role: ["admin"],
        canActive: ["/internal-teacher", "/external-teacher"],
        items: [
          {
            id: '0_2_1',
            parent: '0_2',
            name: 'Giảng viên nội bộ',
            link: '/internal-teacher',
            role: ["admin"]
          },
          {
            id: '0_2_2',
            parent: '0_2',
            name: 'Giảng viên ngoài',
            link: '/external-teacher',
            role: ["admin"]
          },
        ]
      },
      {
        id: '0_3',
        parent: '0',
        name: 'Quản lý chứng chỉ',
        role: ["admin"],
        canActive: ["/internal-badge", "/external-badge"],
        items: [
          {
            id: '0_3_1',
            parent: '0_3',
            name: 'Chứng chỉ nội bộ',
            link: '/internal-badge',
            role: ["admin"]
          },
          {
            id: '0_3_2',
            parent: '0_3',
            name: 'Chứng chỉ ngoài',
            link: '/external-badge',
            role: ["admin"]
          },
        ]
      },
    ],
  },
  {
    id: '1',
    heading: 'Người dùng',
    role: ["admin"],
    items:[
      {
        id: '1_1',
        parent: '1',
        name: 'Quản lý người dùng',
        role: ["admin"],
        canActive: ["/account", "/role", "/permission", "/department", "/position"],
        items: [
          {
            id: '1_1_1',
            parent: '1_1',
            name: 'Tài khoản',
            link: '/account',
            role: ["admin"]
          },
          {
            id: '1_1_2',
            parent: '1_1',
            name: 'Vai trò',
            link: '/role',
            role: ["admin"]
          },
          {
            id: '1_1_3',
            parent: '1_1',
            name: 'Quyền hạn',
            link: '/dermission',
            role: ["admin"]
          },
          {
            id: '1_1_4',
            parent: '1_1',
            name: 'Đơn vị',
            link: '/department',
            role: ["admin"]
          },
          {
            id: '1_1_5',
            parent: '1_1',
            name: 'Chức danh',
            link: '/position',
            role: ["admin"]
          }
        ]
      }
    ] 
  },
  {
    id: '2',
    heading: 'Báo cáo',
    role: ["admin"],
    items:[
      {
        id: '2_1',
        parent: '2',
        name: 'Quản lý báo cáo',
        role: ["admin"],
        canActive: ["/dashboard", "/exam-result", "/course-organization", "/student-progress"],
        items: [
          {
            id: '2_1_1',
            parent: '2_1',
            name: 'Bảng điều khiển',
            link: '/dashboard',
            role: ["admin"]
          },
          {
            id: '2_1_2',
            parent: '2_1',
            name: 'Kết quả thi',
            link: '/exam-result',
            role: ["admin"]
          },
          {
            id: '2_1_3',
            parent: '2_1',
            name: 'Tổ chức khóa học',
            link: '/course-organization',
            role: ["admin"]
          },
          {
            id: '2_1_4',
            parent: '2_1',
            name: 'Tiến độ học viên',
            link: '/student-progress',
            role: ["admin"]
          },
        ]
      }
    ] 
  }
];

export default menuItems;
