import { FC, ReactNode, useState, useContext } from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import { SidebarContext } from 'src/contexts/SidebarContext';

import PropTypes from 'prop-types';
import { Button, Badge, Collapse, ListItem, styled, List, Box } from '@mui/material';

import ExpandLessTwoToneIcon from '@mui/icons-material/ExpandLessTwoTone';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { selectors as selectorApp } from '../../../../store/app';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

interface SidebarMenuItemProps {
  children?: ReactNode;
  link?: string;
  icon?: any;
  badge?: string;
  open?: boolean;
  active?: boolean;
  name: string;
  role?: []
}

const SidebarMenuItem: FC<SidebarMenuItemProps> = ({
  children,
  link,
  icon: Icon,
  badge,
  open: openParent,
  active,
  name,
  role,
  ...rest
}) => {
  const [menuToggle, setMenuToggle] = useState<boolean>(openParent);
  const isOpenSidebar: boolean = useSelector(selectorApp.sideBarSelector);
  const { t } = useTranslation();
  const { toggleSidebar } = useContext(SidebarContext);

  const toggleMenu = (): void => {
    setMenuToggle((Open) => !Open);
  };

  const HeadMenuWrapper = styled(Box)(
    ({ theme }) => `
    display: flex;
    align-items: center;
    justify-content: space-between;
    cursor: pointer;
    .MuiBox-root {
      font-weight: 500;
      padding-left: ${theme.spacing(.8)};
      color: rgb(77, 81, 111);
      line-height: 1.4;
    }
  `
  );

  if (children) {
    return (
      <List className={`Mui-children ${clsx({ 'Mui-active': menuToggle })}`} key={name} {...rest}>
        <HeadMenuWrapper onClick={toggleMenu}>
          {name}
          {menuToggle ? <ExpandLessTwoToneIcon fontSize="small" /> : <NavigateNextIcon fontSize="small" />}
        </HeadMenuWrapper>
        <Collapse in={menuToggle}>{children}</Collapse>
      </List>
    );
  }

  return (
    <ListItem component="div" sx={{
      paddingTop: '5px'
    }} key={name} {...rest}>
      <Button
        className={active ? "Mui-active" : ""}
        component={RouterLink}
        onClick={toggleSidebar}
        to={link}
        startIcon={Icon && <Icon />}
      >
        {/* {isOpenSidebar ? t(`trans_menu.${name}`) : null} */}
        {name}
        {badge && <Badge badgeContent={badge} />}
      </Button>
    </ListItem>
  );
};

SidebarMenuItem.propTypes = {
  children: PropTypes.node,
  active: PropTypes.bool,
  link: PropTypes.string,
  icon: PropTypes.elementType,
  badge: PropTypes.string,
  open: PropTypes.bool,
  name: PropTypes.string.isRequired,
  role: PropTypes.any
};

SidebarMenuItem.defaultProps = {
  open: false,
  active: false,
};

export default SidebarMenuItem;
