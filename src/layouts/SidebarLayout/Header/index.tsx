import { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Box, Hidden, IconButton, Tooltip } from '@mui/material';
import { styled } from '@mui/material';
import { SidebarContext } from 'src/contexts/SidebarContext';
import { actions as actionApp, selectors as selectorApp } from '../../../store/app';
// import HeaderMenu from './Menu';
import HeaderButtons from './Buttons';
import HeaderUserbox from './Userbox';
import Logo from 'src/components/Logo';

const HeaderWrapper = styled(Box, {
  shouldForwardProp: (prop) => prop !== 'open',
})<any>(({ theme, open }) => ({
  height: theme.header.height,
  color: theme.header.textColor,
  padding: theme.spacing(0, 2),
  right: 0,
  zIndex: 5,
  backgroundColor: theme.header.background,
  boxShadow: theme.header.boxShadow,
  position: 'fixed',
  left: theme.sidebar.width,
  justifyContent: 'space-between',
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    left: `calc(${theme.spacing(7)} + 1px)`,
    width: `calc(100% - ${theme.sidebar.width}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const HeaderLogo = styled("div")({ 
  "@keyframes shine": {
    from: {
      maskPosition: "150%"
    },

    to: {
      maskPosition: "-50%"
    }
  },
  maskImage: "linear-gradient(-75deg,rgba(0,0,0,.6) 30%,#000 50%,rgba(0,0,0,.6) 70%)",
  maskSize: "200%",
  animation: "shine 2s infinite"
});

function Header() {
  const { sidebarToggle, toggleSidebar } = useContext(SidebarContext);
  const isOpenSidebar: boolean = useSelector(selectorApp.sideBarSelector);
  const dispatch = useDispatch();

  const collapseSideBar = () => {
    dispatch(actionApp.toggleCollapseSideBar());
  }
  return (
    <HeaderWrapper display="flex" alignItems="center" open={!isOpenSidebar}>
      <Box display="flex" alignItems="center">
        <Hidden lgUp>
          <Logo />
        </Hidden>
      </Box>
      <HeaderLogo>
        <img src="/static/images/logo/logo.png" width="55px" alt="Elearning logo images" />
      </HeaderLogo>
      <Box display="flex" alignItems="center">
        <HeaderButtons />
        <HeaderUserbox />
      </Box>
    </HeaderWrapper>
  );
}

export default Header;
