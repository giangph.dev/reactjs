import { Box, Theme } from '@mui/material';

import { styled } from '@mui/material/styles';
import SidebarMenu from './SidebarMenu';
import HeaderUserbox from '../SidebarLayout/Header/Userbox';

const TabBarContainer = styled(Box, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, $open }: { theme?: Theme, $open: boolean }) => ({
        width: '100%',
        height: theme.header.height,
        color: theme.header.textColor,
        padding: theme.spacing(0, 2),
        zIndex: 5,
        backgroundColor: theme.header.background,
        boxShadow: theme.header.boxShadow,
        position: 'fixed',
        display: 'flex',
        justifyContent: 'space-between'
    }),
);

function TabBar() {
    return (
        <>
            <TabBarContainer $open={true}>
                <Box sx={{ display: 'flex', alignItems: 'center' }}> <SidebarMenu /></Box>
                <Box sx={{
                    display: "flex", alignItems: "center"
                }}><HeaderUserbox /></Box>
            </TabBarContainer>
        </>
    );
}

export default TabBar;
