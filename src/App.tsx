import * as React from 'react';
import { useRoutes } from 'react-router-dom';
import routes from './router/router';
import AdapterDateFns from '@mui/lab/AdapterDateFns';

import ThemeProvider from './theme/ThemeProvider';
import { CssBaseline } from '@mui/material';
import { ToastContainer, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { UserInfoProvider } from './contexts/userInfoContext';

const App = () => {
  const content = useRoutes(routes);
  return (
    <ThemeProvider>
      <CssBaseline />
      <ToastContainer
        theme="colored"
        transition={Zoom}
        position="top-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <UserInfoProvider>
        {content}
      </UserInfoProvider>
    </ThemeProvider>
  );
}
export default App;
