import axios from "axios";
import api from "./abstract.service";
const login = async (data: any) => {
  const result = await axios.post(`${process.env.REACT_APP_API_ROOT_URL}api/v1/login`, data).then(res => {
    const token = res.data?.token;
    if(data && token) {
      localStorage.setItem('accessToken', token.accessToken);
      localStorage.setItem('refreshToken', token.refreshToken);
    }
    return res.data;
  });
  return result;
}

const logout = () => {
  localStorage.clear();
  // msalInstance.logoutRedirect({postLogoutRedirectUri: "/auth"});
};


export {
  login,
  logout,
};