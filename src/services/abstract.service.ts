import axios from 'axios';
import qs from 'qs';
import { CONFIG_APP } from 'src/constants/common';

const api = axios.create({
  baseURL: CONFIG_APP.PUBLIC_URL,
  paramsSerializer: params => qs.stringify(params, { arrayFormat: 'repeat' }),
  timeout: 100000,
});

api.interceptors.request.use(
  async config => {
    const idToken: string = JSON.parse(localStorage.getItem('accessToken')).token.toString();
    const common: any = {
      Accept: 'application/json, text/plain, */*',
      Authorization: `Bearer ${idToken}`,
      //more settings
    };

    config.headers = common;

    return config;
  },
  error => {
    Promise.reject(error);
  },
);

api.interceptors.response.use(
  res => {
    return res;
  }
);

export default api;