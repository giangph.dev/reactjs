import axios from "axios";
import api from "./abstract.service";

const getDepartment = async (data: any) => {
  // const result = await axios.post(`${process.env.REACT_APP_API_ROOT_URL}api/v1/department`, data).then(res => {
  //   return res.data;
  // });
  // return result;
  return {data: [
    {
      id: 0,
      name: 'Phòng ban 1',
      code: 'pv1',
      parentName: 'Marketting',
      status: 1,
      ternantName: 'viettel'
    },
    {
      id: 1,
      name: 'Phòng ban 2',
      code: 'pv2',
      parentName: 'Công nghệ',
      status: 1,
      ternantName: 'viettel'
    },
    {
      id: 2,
      name: 'Phòng ban 3',
      code: 'pv3',
      parentName: 'WV',
      status: 1,
      ternantName: 'viettel'
    },
    {
      id: 3,
      name: 'Phòng ban 4',
      code: 'pv4',
      parentName: 'Kinh doanh',
      status: 1,
      ternantName: 'viettel'
    },
    {
      id: 4,
      name: 'Phòng ban 5',
      code: 'pv5',
      parentName: 'Phòng ban 1',
      status: 1,
      ternantName: 'viettel'
    },
    {
      id: 5,
      name: 'Phòng ban 6',
      code: 'pv6',
      parentName: 'WC',
      status: 1,
      ternantName: 'viettel'
    }
  ]}
};

export const services = {
  getDepartment: getDepartment
}