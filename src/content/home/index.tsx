import { memo } from 'react';
import { Main } from './components/loadable';
const Home = memo(() => {
  return (
    <>
      <Main />
    </>
  )
})

export default Home