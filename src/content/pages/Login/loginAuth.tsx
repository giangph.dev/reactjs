import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate } from 'react-router';
import { actions as actionAuth, selectors as selectorAuth } from '../../../store/auth';
// import { useMsal } from "@azure/msal-react";
import { Box, CircularProgress, Typography } from '@mui/material';
import { oauth2Google } from 'src/constants/auth.config';
import axios from 'axios';

export default function LoginAuth() {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const dispatch = useDispatch();
  const isLoggedIn = useSelector(selectorAuth.userSelector);
  const isError = useSelector(selectorAuth.errorSelector);
  const message = useSelector(selectorAuth.messageSelector);

  const getAllParams = () => {
    const params = {};
    searchParams.forEach((value: any, key: string) => {
      params[key] = value;
    });

    return params;
  }

  const data = getAllParams();

  const handleGoogleSiged = async (redirect_data) => {
    const { client_id, redirect_uri } = oauth2Google;
    const { code } = redirect_data;
    axios.get(`http://elearning.local.com:3000/auth/google?code=${code}`).then((res) => {
      if(res) {
        console.log(res);
      }
    })
  }

  useEffect(() => {
    if(data) {
      handleGoogleSiged(data);
    }
  }, []);

  if (isLoggedIn) {
    return <Navigate to='/dashboard' />;
  }

  return <Box display={'flex'} flexDirection={'column'} justifyContent={'center'} alignItems={'center'} sx={{height: '100%'}}>
    {
      !isError ? <Box>
        <CircularProgress />
      </Box> : null
    }
  </Box>
}