import { useRef } from 'react';
import MainContent from './mainContent';
import { Navigate } from 'react-router';
import { useUserInfo } from 'src/contexts/userInfoContext';

const Main = ({
  login,
  isLoggedIn
}: any) => {
  
  if(isLoggedIn) {
    return <Navigate to="/home" />;
  }
  
  const ref = useRef();
  const { updateUserInfo } = useUserInfo();
  const handleSubmitLogin = async (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    const result = await login({
      username: data.get('email'),
      password: data.get('password'),
      terannt: 1
    });
    // Set account context
    if(result && result[0]) {
      const userInfo = result[0].data;
      updateUserInfo(userInfo);
    }
  };

  const handleGoogleLogin = () => {

  };
  

  return (
    <MainContent 
      login={login}
      submitLogin={handleSubmitLogin}
      googleLogin={handleGoogleLogin}
    />
  )
};

export default Main;