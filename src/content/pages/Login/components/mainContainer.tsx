import { connect } from 'react-redux';
import { actions, selectors } from 'src/store/auth';
import Main from './main';

const mapStateToProps = (state: any) => ({
  isLoggedIn: selectors.userSelector(state)
});
const mapDispatchToProps = dispatch => ({
  login: async (data) => {
    return await Promise.all([
      dispatch(actions.loginAuth(data))
    ])
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
