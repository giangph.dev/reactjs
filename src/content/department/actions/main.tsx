import loadable from 'src/components/Loadable';

export const CreateAction = loadable(
  () => import('./mainContainers')
);