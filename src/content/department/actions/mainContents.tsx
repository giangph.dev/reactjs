import { Fragment, forwardRef, FC } from 'react';
import {
  Grid,
  Box,
  Button,
  DialogActions,
  DialogContent,
  Dialog,
  DialogTitle,
  IconButton,
} from '@mui/material';
import CancelRoundedIcon from '@mui/icons-material/CancelRounded';
import { CreateAction } from './main';
interface iRecipeProps {
  columns: any[],
  activeStep: any,
  completed: any,
  handleStep: any,
  handleClose: any,
  formik: any,
  deviceData: any[],
  onClose: any,
  deviceIds: string,
  open: boolean,
  t: any,
  data: any[],
  softwareData: any[],
  changeSelectedSoftWare: any,
  handleSubmit: any
}
const MainContent: FC<iRecipeProps> = forwardRef(({
  handleClose,
  handleSubmit,
  open,
  formik,
  t
}, ref): JSX.Element => {

  return (
    <>
      <Dialog maxWidth={"md"} fullWidth onClose={handleClose} open={open}>
        <Box component="form" noValidate>
          <DialogTitle>
            <Box display="flex" alignItems="center">
              <Box flexGrow={1}>Create department</Box>
              <Box>
                <IconButton onClick={handleClose}>
                  <CancelRoundedIcon />
                </IconButton>
              </Box>
            </Box>
          </DialogTitle>
          <DialogContent sx={{ height: 500 }}>
            <Box>
              <Grid>
                
              </Grid>
            </Box>
          </DialogContent>
          <DialogActions>
            <Button variant="contained" onClick={handleClose}>{t('action.cancel')}</Button>
            <Button variant="outlined">{t('action.next')}</Button>
          </DialogActions>
        </Box>
      </Dialog>
    </>
  )
});

export default MainContent;