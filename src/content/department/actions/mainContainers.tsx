import { useState, useEffect } from 'react';
import MainContent from './mainContents';
import { withTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate, useNavigate } from 'react-router';

function MainContainer(props: any) {
  const { selectedValue, onClose } = props;
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const steps = ['Select update options', 'Confirm devices will be updated'];
  const [activeStep, setActiveStep] = useState(0);
  const [completed, setCompleted] = useState<{
    [k: number]: boolean;
  }>({})

  const formik = {
    step1: useFormik({
      initialValues: {
        software: '',
        version: '',
        timeReason: new Date(),
        checkFail: false,
        deviceId: props.deviceIds
      },
      onSubmit: async (values) => {
        setCompleted({});
        console.log(values);
        navigate("/department");
      },
    })
  }

  const handleClose = () => {
    formik.step1.resetForm();
    onClose(selectedValue);
    setActiveStep(0)
  };

  useEffect(() => {

  }, []);


  return (
    <MainContent
      {...props}
      activeStep={activeStep}
      completed={completed}
      handleClose={handleClose}
      formik={formik}
      data={steps}
    />
  )
};

export default withTranslation()(MainContainer);