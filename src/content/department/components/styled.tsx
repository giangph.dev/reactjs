import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
const MainContentWrapper = styled(Box)(
  ({ theme }) => `
    .head {
      display: flex;
      align-items: center;
      justify-content: end;
    }
  `
);

export {
  MainContentWrapper,
}