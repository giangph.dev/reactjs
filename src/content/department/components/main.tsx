import { useEffect, useRef, useState } from 'react';
import MainContent from './mainContent';
import { Box, ButtonGroup, Chip, IconButton } from '@mui/material';
import { Add, EditAttributes, Edit, Delete } from '@mui/icons-material';

const Main = ({
  init,
  department
}: any) => {
  const ref = useRef();
  const [pageSize, setPageSize] = useState(10);
  const [rowCount] = useState(0);
  const [pageIndex, setPageIndex] = useState(1);
  const [keyword, setKeyword] = useState("");
  const [ids, setIds] = useState([]);
  const [isError, setError] = useState(false);
  const [totalPage, setTotalPage] = useState(20);
  const [openAction, setOpenAction] = useState(false);
  const headCells = [
    {
      headerName: 'Tên đơn vị',
      field: 'name',
      sort: true
    },
    {
      headerName: 'Mã đơn vị',
      field: 'code',
      sort: true
    },
    {
      headerName: 'Trực thuộc đơn vị',
      field: 'parentName',
      sort: true,
    },
    {
      headerName: 'Trạng thái kích hoạt',
      field: 'status',
      type: 'custom',
      renderCell: (cellValues: any) => {
        const value = cellValues.status;
        return <Chip size='small' label={['Disable', 'Enabel'][value]} color={value ? 'success' : 'warning'} />
      }
    },
    {
      headerName: 'Tổng bộ',
      field: 'ternantName',
    },
    {
      field: 'actions',
      type: 'custom',
      headerName: 'Actions',
      renderCell: (cellValues: any) => {
        return <Box display={'flex'} alignItems={'center'}>
          <ButtonGroup size='small' variant="outlined" sx={{
            border: "1px solid #ddd",
          }}>
            {/* Active action */}
            <IconButton color={cellValues.status ? 'success' : 'secondary'} sx={{
              // borderRight: '1px solid #ddd',
              borderRadius: 0
            }}>
              <EditAttributes fontSize='small' />
            </IconButton >
            {/* Edit action */}
            <IconButton sx={{
              // borderRight: '1px solid #ddd',
              borderRadius: 0
            }}>
              <Edit fontSize='small' />
            </IconButton >
            {/* Delete action */}
            <IconButton sx={{
              borderRight: '1px solid #ddd',
              borderRadius: 0
            }}>
              <Delete fontSize='small' />
            </IconButton >
          </ButtonGroup>
        </Box>
      }
    }
  ];
  
  useEffect(() => {
    init([]);
  }, [keyword, pageSize, pageIndex])

  return (
    <MainContent
      openAction={openAction}
      setOpenAction={setOpenAction}
      department={department}
      headCells={headCells}
      ref={ref}
      pageSize={pageSize}
      page={pageIndex}
      totalPage={totalPage}
      rowCount={rowCount}
      ids={ids}
      setIds={setIds}
      setPageIndex={setPageIndex}
      setPageSize={setPageSize}
      setKeyword={setKeyword}
    />
  )
};

export default Main;