import { forwardRef, FC } from 'react';
import { Button, TextField, Link, Grid, Box, Typography } from '@mui/material';
import { MainContentWrapper } from './styled';
import TableCustom from 'src/components/TableCustom';
import { Add } from '@mui/icons-material';
import { CreateAction } from '../actions/main';

interface iRecipeProps {
  openAction: boolean;
  setOpenAction: any;
  department: any[];
  headCells: any[];
  ref: any;
  pageSize: number;
  page: number;
  totalPage: number;
  rowCount: number;
  setPageIndex: any;
  setPageSize: any;
  setKeyword: any;
  ids: any[];
  setIds: any;
}

const MainContent: FC<iRecipeProps> = forwardRef(
  (
    {
      openAction,
      setOpenAction,
      department,
      headCells,
      pageSize,
      page,
      totalPage,
      rowCount,
      setPageIndex,
      setPageSize,
      setKeyword,
      ids,
      setIds
    },
    ref
  ): JSX.Element => {
    return (
      <MainContentWrapper className="mainContentWrapper">
        <Box className="headerWrapper" sx={{ mb: 2 }}>
          <Button
            onClick={() => {
              setOpenAction(true);
            }}
            startIcon={<Add fontSize="small" />}
            variant="contained"
            color="success"
          >
            Thêm mới
          </Button>
        </Box>
        <Box className="bodyWrapper">
          <TableCustom
            dataTable={department}
            idKey="id"
            header={headCells}
            pageSize={pageSize}
            pageIndex={page}
            selected={ids}
            setSelected={setIds}
          />
        </Box>
        {openAction ? (
          <CreateAction
            open={openAction}
            onClose={() => {
              setOpenAction(false);
            }}
            id={ids}
          />
        ) : null}
      </MainContentWrapper>
    );
  }
);

export default MainContent;
