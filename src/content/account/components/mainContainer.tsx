import { connect } from 'react-redux';
import { actions, selectors } from 'src/store/department';
import Main from './main';

const mapStateToProps = (state: any) => ({
  // devices: selectors.departmentSelector(state),
});
const mapDispatchToProps = dispatch => ({
  // init: async (searchData: string) => {
  //   return await Promise.all([
  //     dispatch(actions.getDepartment(searchData)),
  //   ]);
  // },
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
