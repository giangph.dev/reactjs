import { forwardRef, FC } from "react";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { getGoogleUrl } from 'src/utils/common';
import { MainContentWrapper } from "./styled";
import FormInput from "src/components/FormInput";

interface iRecipeProps {
}

const MainContent: FC<iRecipeProps> = forwardRef(({
  
}, ref): JSX.Element => {
  return (
    <>
      <MainContentWrapper className="mainContentWrapper">
        <Box className="head">
          <Button variant="contained">Tạo tài khoản</Button>
        </Box>
        <Box className="tableWrapper">
          <Grid container>
            <Grid xs={2} item>
              <Box className="filter">
                <FormInput 
                  noLable={true}
                  value="" 
                  placeholder="Nhập tên tài khoản" 
                  type="textbox"
                />
              </Box>
            </Grid>
            <Grid xs={10} item>

            </Grid>
          </Grid>
        </Box>
      </MainContentWrapper>
    </>
  );
});

export default MainContent;