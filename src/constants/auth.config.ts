
export const loginRequest = {
    scopes: [
       
    ]
};

/**
 * An optional silentRequest object can be used to achieve silent SSO
 * between applications by providing a "login_hint" property.
 */
export const silentRequest = {
    scopes: ["openid", "profile"],
    loginHint: "example@domain.net"
};

export const oauth2Google = {
    redirect_uri: process.env.REACT_APP_GOOGLE_OAUTH_REDIRECT,
    client_id: process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_ID,
    secret_id: process.env.REACT_APP_GOOGLE_OAUTH_CLIENT_SECRET
}