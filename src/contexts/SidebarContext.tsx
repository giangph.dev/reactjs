import { FC, useState, createContext } from 'react';
type SidebarContext = { sidebarToggle: boolean; toggleSidebar: () => void };


export const SidebarContext = createContext<SidebarContext>(
  {} as SidebarContext
);

interface Props {
  children: React.ReactNode;
}

export const SidebarProvider: FC<Props> = ({ children }) => {
  const [sidebarToggle, setSidebarToggle] = useState<boolean>(false);
  const toggleSidebar = () => {
    setSidebarToggle(!sidebarToggle);
  };

  return (
    <SidebarContext.Provider value={{ sidebarToggle, toggleSidebar }}>
      {children}
    </SidebarContext.Provider>
  );
};
